/*
 * Display.c
 *
 *  Created on: Dec 8, 2017
 *      Author: angusschmaloer
 *
 *	User's functions, application specific
 *	The function names explains everything aboutthe funcitons
 */

#include "User/Display.h"

void SSD1306DisplayUpdataRPM(signed int RPM){
	uint8_t tempdata[6];

	if(RPM < 0){
		tempdata[5] = 'L';
		RPM = -RPM;
	}else if(RPM > 0){
		tempdata[5] = 'R';
	}else{
		tempdata[5] = ' ';
	}
	tempdata[3] = RPM%10;
	tempdata[2] = (RPM/10)%10;
	tempdata[1] = (RPM/100)%10;
	tempdata[0] = (RPM/1000)%10;
	tempdata[4] = ' ';

	SSD1306Display(tempdata, sizeof(tempdata), 6, 1);
}

void SSD1306DisplaySetupRPM(void){
	uint8_t tempdata[5] = "rpm  ";
	SSD1306Display(tempdata, sizeof(tempdata), 1, 1);
	SSD1306DisplayUpdataRPM(0);
}


void SSD1306DisplayUpdataState(int state){
	uint8_t tempdata0[10] = "Running   ";
	uint8_t tempdata1[10] = "Setting up";
	uint8_t tempdata2[10] = "Updating  ";
	switch(state){
	case 0:
		SSD1306Display(tempdata0, sizeof(tempdata0), 6, 0);
		break;
	case 1:
		SSD1306Display(tempdata1, sizeof(tempdata1), 6, 0);
		break;
	case 2:
		SSD1306Display(tempdata2, sizeof(tempdata2), 6, 0);
		break;
	}
}

void SSD1306DisplaySetupState(void){
	uint8_t tempdata[6] = "State ";
	SSD1306Display(tempdata, sizeof(tempdata), 0, 0);
	SSD1306DisplayUpdataState(1);
}

void SSD1306DisplayMenuPointer(int state, int selection){
	uint8_t tempdata;
	uint8_t tempfilldata = ' ';
	switch(selection){
	case 0:
		tempdata = ' ';
		break;
	case 1:
		tempdata = 42;
		break;
	case 2:
		tempdata = 43;
		break;
	}

	switch(state){
	case 0:
		SSD1306Display(&tempdata, 	  sizeof(tempdata), 	0, 1);
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 0, 2);
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 0, 3);
		break;
	case 1:
		SSD1306Display(&tempdata, 	  sizeof(tempdata), 	0, 2);
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 0, 1);
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 0, 3);
		break;
	case 2:
		SSD1306Display(&tempdata, 	  sizeof(tempdata), 	0, 3);
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 0, 1);
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 0, 2);
		break;
	}
}

void SSD1306DisplaySetupStart(void){
	uint8_t tempdata[5] = "Start";
	SSD1306Display(tempdata, sizeof(tempdata), 1, 3);
}

void SSD1306DisplaySetupStop(void){
	uint8_t tempdata[5] = "Stop ";
	SSD1306Display(tempdata, sizeof(tempdata), 1, 3);
}

void SSD1306DisplaySetupRIP(bool high){
	uint8_t tempdata1[4] = "High";
	uint8_t tempdata2[4] = "Low ";
	if(high){
		SSD1306Display(tempdata1, sizeof(tempdata1), 15 - sizeof(tempdata1), 3);
	}else{
		SSD1306Display(tempdata2, sizeof(tempdata2), 15 - sizeof(tempdata2), 3);

	}
}

void SSD1306DisplaySetupTime(void){
	uint8_t tempdata[4] = "Time";
	SSD1306Display(tempdata, sizeof(tempdata), 1, 2);
	SSD1306DisplayUpdataTimeHours(0);
	SSD1306DisplayUpdataTimeMins(0);
	SSD1306DisplayUpdataTimeSecs(0);
}

void SSD1306DisplayTimerPointer(int x){
	uint8_t tempdata = 42;
	uint8_t tempfilldata = ' ';
	switch(x){
	case 0:
		SSD1306Display(&tempdata, 	  sizeof(tempdata), 	5, 2);
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 8, 2);
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 11,2);
		break;
	case 1:
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 5, 2);
		SSD1306Display(&tempdata,	  sizeof(tempdata), 	8, 2);
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 11,2);
		break;
	case 2:
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 5, 2);
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 8, 2);
		SSD1306Display(&tempdata, 	  sizeof(tempdata), 	11,2);
		break;
	case 4:
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 5, 2);
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 8, 2);
		SSD1306Display(&tempfilldata, sizeof(tempfilldata), 11,2);
		break;
	}
}

void SSD1306DisplayUpdataTimeSecs(uint8_t time){
	uint8_t tempdata[2];
	tempdata[1] = time%10;
	tempdata[0] = (time/10)%10;
	SSD1306Display(tempdata, sizeof(tempdata), 12, 2);
}
void SSD1306DisplayUpdataTimeMins(uint8_t time){
	uint8_t tempdata[2];
	tempdata[1] = time%10;
	tempdata[0] = (time/10)%10;

	SSD1306Display(tempdata, sizeof(tempdata), 9, 2);
}
void SSD1306DisplayUpdataTimeHours(uint8_t time){
	uint8_t tempdata[2];
	tempdata[1] = time%10;
	tempdata[0] = (time/10)%10;
	SSD1306Display(tempdata, sizeof(tempdata), 6, 2);
}

