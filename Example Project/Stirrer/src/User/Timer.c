/*
 * Timer.c
 *
 *  Created on: Dec 8, 2017
 *      Author: angusschmaloer
 */


#include "Driver/TIM.h"
#include "User/Timer.h"
#include "Driver/SSD1306.h"
#include "User/Display.h"

signed int MotorSetpoint 	= 0;
signed int MotorFreq 		= 0;

#define ON 	0
#define OFF 1
bool timing 	= false;

int *timeSecs  = 0;
int *timeMins  = 0;
int *timeHours = 0;

enum SwitchState _SwitchState = SettingUp;
enum MenuState   _MenuState   = RPM;

signed int rpm = 0;

enum TimerJob{
	Idle,
	Update,
	Timing
};
enum TimerJob _TimerJob = Idle;

void TIM2_Initialization(void){
	TIM2_Init(&TIM2_Handle);
}

void TIM4_Initialization(void){
	TIM4_Init(&TIM4_Handle);
}

void Timer4_Callback(){
	/* if the timer job is timing, then count up every callback, update the display and set the setpoint to 0 when the counter is 0, so the stirrer will stop*/
	if(_TimerJob == Timing){
		if(!timing){
			return;
		}
		if((*timeSecs) == 0){
			if((*timeMins) == 0){
				if((*timeHours) == 0){
					rpm = 0;
					SSD1306DisplayUpdataRPM(rpm);
					_SwitchState = Updating;
					SSD1306DisplayUpdataState(_SwitchState);
					UpdateRPM(0);
					return;
				}
				(*timeMins) = 60;
				(*timeHours)--;
				SSD1306DisplayUpdataTimeHours(*timeHours);
			}
			(*timeSecs) = 60;
			(*timeMins)--;
			SSD1306DisplayUpdataTimeMins(*timeMins);
		}
		(*timeSecs)--;
		SSD1306DisplayUpdataTimeSecs(*timeSecs);
	}
	/* if the timer job is updating, then ramp the rpm value every callback with 10 at 1kHz (213.33 is 1 RPM), so it ramps at 10000/213.33 = 46.9 rpm per second  */
	else if(_TimerJob == Update){
		if(MotorFreq > MotorSetpoint+5){			//RPM to Hight
			MotorFreq -= 10;
			if(MotorFreq < 0){
				UpdateMotor(-MotorFreq, 0);
			}else if(MotorFreq >= 0){
				UpdateMotor(MotorFreq, 1);
			}
		}else if(MotorFreq < MotorSetpoint-5){	//RPM to Low
			MotorFreq += 10;
			if(MotorFreq < 0){
				UpdateMotor(-MotorFreq, 0);
			}else if(MotorFreq >= 0){
				UpdateMotor(MotorFreq,1);
			}
		}else{
			if(timing){
				_SwitchState = Running;
				SSD1306DisplayUpdataState(_SwitchState);
				StartTimer();
			}else{
				if(rpm == 0){
					_SwitchState = SettingUp;
					SSD1306DisplayUpdataState(_SwitchState);
					SSD1306DisplaySetupStart();
					_MenuState = Start;
				}else{
					_SwitchState = Running;
					SSD1306DisplayUpdataState(_SwitchState);
					SSD1306DisplaySetupStop();
					_MenuState = Stop;
				}

				HAL_TIM_Base_Stop_IT(&TIM4_Handle);
				_TimerJob = Idle;
			}
		}
	}
}

/*
 * Sets the PWM output to 0,
 * and start it again
 */
void Restart(void){
	MotorFreq = 0;
	UpdateMotor(MotorFreq,1);
	UpdateRPM((MotorSetpoint*60)/12800);
}

/*
 * Sets the time
 */
void SetTime(int *secs, int *mins, int *hours){
	timeSecs  = secs;
	timeMins  = mins;
	timeHours = hours;
}

/*
 * Starts the Timer
 * Sets the tim4 to count for the counter
 */
void StartTimer(){
	TIM4_Handle.Instance->CNT  = 0;
	TIM4_Handle.Instance->PSC  = 8000 - 1;
	TIM4_Handle.Instance->ARR  = 1000 - 1;
	TIM4_Handle.Instance->CNT  = 0;
	if(_TimerJob == Idle){
		HAL_TIM_Base_Start_IT(&TIM4_Handle);
	}
	_TimerJob = Timing;
}

/*
 * Updates the RPM Set point
 */
void UpdateRPM(signed int _RPM){
	MotorSetpoint = 12800*_RPM/60;			//60 rpm = 12800Hz, so we take 12800/60

	/* set timing true if there is any time is set*/
	if(((*timeSecs) + (*timeMins) + (*timeHours) > 0) && (_RPM != 0)){
		timing = true;
	}else{
		timing = false;
	}

	/* if the new set point is not the same as the current RPM value,
	 * then set the tim4 for the ramping
	 *  */
	if(MotorFreq != MotorSetpoint){
		if(_TimerJob != Update){
			TIM4_Handle.Instance->CNT  = 0;
			TIM4_Handle.Instance->PSC  = (8000000/100000) - 1; 	//100kHz
			TIM4_Handle.Instance->ARR  = (100000/1000) - 1;		//  1kHz
			TIM4_Handle.Instance->CNT  = 0;
			if(_TimerJob == Idle){
				HAL_TIM_Base_Start_IT(&TIM4_Handle);
			}
			_TimerJob = Update;
		}
	}
	/* if the RP value is the same and  timing is true, then start timer. */
	else if(timing){
		StartTimer();
	}
}

/*
 * Pause timer, timing and ramping
 * the RPM/PWM will stay the same
 */
void Pause(){
	HAL_TIM_Base_Stop_IT(&TIM4_Handle);
	_TimerJob = Idle;
}

/*
 * Update the RPM value from Tim2
 */
void UpdateMotor(int RPM, bool _Polarity){
	static bool on = false;
	if(RPM == 0 ){
		if(on){
			HAL_GPIO_WritePin(EnablePORT, EnablePin, OFF);
			HAL_TIM_PWM_Stop(&TIM2_Handle, TIM_CHANNEL_1);
			on = false;
		}
	}else{
		TIM2_Handle.Instance->CNT  = 0;
		TIM2_Handle.Instance->CCR1 = (4000000/RPM)/2;
		TIM2_Handle.Instance->ARR  = (4000000/RPM) - 1;
		TIM2_Handle.Instance->CNT  = 0;
		if(!on){
			HAL_GPIO_WritePin(EnablePORT, EnablePin, ON);
			HAL_TIM_PWM_Start(&TIM2_Handle, TIM_CHANNEL_1);
			on = true;
		}
		HAL_GPIO_WritePin(PolarityPORT, PolarityPin, _Polarity);
	}
}
