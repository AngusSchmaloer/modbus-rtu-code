/*
 * RotaryEncoder.c
 *
 *  Created on: Dec 8, 2017
 *      Author: angusschmaloer
 */

#include "User/RotaryEncoder.h"
#include "User/Timer.h"
#include "User/Display.h"
#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"

//#include "stdbool.h"

#define	RotaryEncoderPORT 	 GPIOA
#define	RotaryEncoderCLK  	 GPIO_PIN_5
#define	RotaryEncoderDT   	 GPIO_PIN_6
#define	RotaryEncoderSW   	 GPIO_PIN_7
#define	RotaryEncoderCurrent GPIO_PIN_8


enum time{
	uren,
	minuten,
	seconden
};

enum time _time;

int Secs;
int Mins;
int Hours;

extern signed int rpm;

extern enum SwitchState _SwitchState;
extern enum MenuState   _MenuState;

void RotaryEncoderInit(void)
{
	GPIO_InitTypeDef 			GPIO_InitStruct;

	__HAL_RCC_GPIOA_CLK_ENABLE();

	GPIO_InitStruct.Mode      	= GPIO_MODE_IT_FALLING; //
	GPIO_InitStruct.Pull      	= GPIO_PULLDOWN;
	GPIO_InitStruct.Speed     	= GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pin       	= RotaryEncoderCLK;
	HAL_GPIO_Init(RotaryEncoderPORT, &GPIO_InitStruct);

	GPIO_InitStruct.Mode      	= GPIO_MODE_IT_FALLING; //
	GPIO_InitStruct.Pin       	= RotaryEncoderSW;
	HAL_GPIO_Init(RotaryEncoderPORT, &GPIO_InitStruct);

	GPIO_InitStruct.Mode      	= GPIO_MODE_IT_RISING_FALLING; //
	GPIO_InitStruct.Pin       	= RotaryEncoderCurrent;
	HAL_GPIO_Init(RotaryEncoderPORT, &GPIO_InitStruct);

	GPIO_InitStruct.Mode      	= GPIO_MODE_INPUT;
	GPIO_InitStruct.Pin       	= RotaryEncoderDT;
	HAL_GPIO_Init(RotaryEncoderPORT, &GPIO_InitStruct);

	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 12, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	int direction = 0;
	bool Switch = 0;

	if(GPIO_Pin == GPIO_PIN_8){
		SSD1306DisplaySetupRIP(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_8));
		return;
	}

	if(GPIO_Pin == GPIO_PIN_5){
		if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_6)){
			direction = 2;
		}else{
			direction = 1;
		}
	}
	else if(GPIO_Pin == GPIO_PIN_7){Switch = true;}


	switch(_SwitchState){
	case Running:
		switch(_MenuState){
			case Volume:
				break;
			case _Volume:
				break;
			case _RPM:
				;
				break;
			case _Time:
				;
				break;
			case Start:
				;
				break;
			case RPM:
				if(direction == 1){
					_MenuState = Time;
					SSD1306DisplayMenuPointer(1,1);
				}else if(direction == 2){
					_MenuState = Stop;
					SSD1306DisplayMenuPointer(2,1);
				}
				else if(Switch){
					_SwitchState = SettingUp;
					Pause();
					SSD1306DisplaySetupStart();
					SSD1306DisplayUpdataState(_SwitchState);
					SSD1306DisplayMenuPointer(0,1);
					_MenuState = RPM;
				}
				break;
			case Time:
				if(direction == 1){
					_MenuState = Stop;
					SSD1306DisplayMenuPointer(2,1);
				}else if(direction == 2){
					_MenuState = RPM;
					SSD1306DisplayMenuPointer(0,1);
				}
				else if(Switch){
					_SwitchState = SettingUp;
					Pause();
					SSD1306DisplaySetupStart();
					SSD1306DisplayUpdataState(_SwitchState);
					SSD1306DisplayMenuPointer(1,1);
					_MenuState = Time;
					_time = uren;

				}
				break;
			case Stop:
				if(direction == 1){
					_MenuState = RPM;
					SSD1306DisplayMenuPointer(0,1);
				}else if(direction == 2){
					_MenuState = Time;
					SSD1306DisplayMenuPointer(1,1);
				}
				else if(Switch){
					Secs = 0;
					Mins = 0;
					Hours = 0;
					SetTime(&Secs, &Mins, &Hours);

					SSD1306DisplayUpdataTimeSecs(Secs);
					SSD1306DisplayUpdataTimeMins(Mins);
					SSD1306DisplayUpdataTimeHours(Hours);

					rpm = 0;
					UpdateRPM(rpm);
					SSD1306DisplayUpdataRPM(rpm);

					_SwitchState = Updating;
					SSD1306DisplayUpdataState(_SwitchState);
					SSD1306DisplaySetupStart();
					SSD1306DisplayMenuPointer(3,1);
					_MenuState = Start;
				}
				break;
		}
		break;
	case SettingUp:
		switch(_MenuState){
			case Stop:
				;
				break;
			case RPM:
				if(direction == 1){
					_MenuState = Time;
					SSD1306DisplayMenuPointer(1,1);
				}else if(direction == 2){
					_MenuState = Start;
					SSD1306DisplayMenuPointer(2,1);
				}
				else if(Switch){
					_MenuState = _RPM;
					SSD1306DisplayMenuPointer(0,2);
				}
				break;
			case _RPM:
				if(direction == 1){
					if(rpm > -1000){
						rpm -= 10;
						SSD1306DisplayUpdataRPM(rpm);
					}
				}else if(direction == 2){
					if(rpm < 1000){
						rpm += 10;
						SSD1306DisplayUpdataRPM(rpm);
					}
				}
				else if(Switch){
					_MenuState = RPM;
					SSD1306DisplayMenuPointer(0,1);
				}
				break;
			case Volume:
				if(direction == 1){
					_MenuState = Time;
					SSD1306DisplayMenuPointer(1,1);
				}else if(direction == 2){
					_MenuState = Start;
					SSD1306DisplayMenuPointer(2,1);
				}
				else if(Switch){
					_MenuState = _RPM;
					SSD1306DisplayMenuPointer(0,2);
				}
				break;
			case _Volume:
				if(direction == 1){
					if(rpm > -1000){
						rpm -= 10;
						SSD1306DisplayUpdataRPM(rpm);
					}
				}else if(direction == 2){
					if(rpm < 1000){
						rpm += 10;
						SSD1306DisplayUpdataRPM(rpm);
					}
				}
				else if(Switch){
					_MenuState = RPM;
					SSD1306DisplayMenuPointer(0,1);
				}
				break;
			case Time:
				if(direction == 1){
					_MenuState = Start;
					SSD1306DisplayMenuPointer(2,1);
				}else if(direction == 2){
					_MenuState = RPM;
					SSD1306DisplayMenuPointer(0,1);
				}
				else if(Switch){
					_MenuState = _Time;
					SSD1306DisplayMenuPointer(1,2);
					_time = uren;
					SSD1306DisplayTimerPointer(_time);
				}
				break;
			case _Time:
				switch(_time){
				case uren:
					if(direction == 2){
						if(Hours == 59){
							Hours = 0;
						}else{
							Hours++;
						}
						SSD1306DisplayUpdataTimeHours(Hours);
					}else if(direction == 1){
						if(Hours == 0){
							Hours = 59;
						}else{
							Hours--;
						}
						SSD1306DisplayUpdataTimeHours(Hours);
					}
					else if(Switch){
						_time = minuten;
						SSD1306DisplayTimerPointer(_time);
					}
					break;
				case minuten:
					if(direction == 2){
						if(Mins == 59){
							Mins = 0;
						}else{
							Mins++;
						}
						SSD1306DisplayUpdataTimeMins(Mins);
					}else if(direction == 1){
						if(Mins == 0){
							Mins = 59;
						}else{
							Mins--;
						}
						SSD1306DisplayUpdataTimeMins(Mins);
					}
					else if(Switch){
						_time = seconden;
						SSD1306DisplayTimerPointer(_time);
					}
					break;
				case seconden:
					if(direction == 2){
						if(Secs == 59){
							Secs = 0;
						}else{
							Secs++;
						}
						SSD1306DisplayUpdataTimeSecs(Secs);
					}else if(direction == 1){
						if(Secs == 0){
							Secs = 59;
						}else{
							Secs--;
						}
						SSD1306DisplayUpdataTimeSecs(Secs);
					}
					else if(Switch){
						_MenuState = Time;
						SSD1306DisplayMenuPointer(1,1);
						_time = uren;
						SSD1306DisplayTimerPointer(4);
					}
					break;
				}
				break;
			case Start:
				if(direction == 1){
					_MenuState = RPM;
					SSD1306DisplayMenuPointer(0,1);
				}else if(direction == 2){
					_MenuState = Time;
					SSD1306DisplayMenuPointer(1,1);
				}
				else if(Switch){
					SSD1306DisplayMenuPointer(3,1);
					SetTime(&Secs, &Mins, &Hours);

					//Secs + Mins + Hours > 0 &&
					if(rpm != 0){
						_SwitchState = Updating;
						SSD1306DisplayUpdataState(_SwitchState);
						_MenuState = Stop;
						SSD1306DisplaySetupStop();
					}else{
						_SwitchState = SettingUp;
						SSD1306DisplayUpdataState(_SwitchState);
						_MenuState = Start;
						SSD1306DisplaySetupStart();
					}



					UpdateRPM(rpm);
				}
				break;
		}
		break;
	case Updating:

		switch(_MenuState){
			case Volume:
				break;
			case _Volume:
				break;
			case _RPM:
				;
				break;
			case _Time:
				;
				break;
			case Start:
				;
				break;
			case RPM:
				if(direction == 1){
					_MenuState = Time;
					SSD1306DisplayMenuPointer(1,1);
				}else if(direction == 2){
					_MenuState = Stop;
					SSD1306DisplayMenuPointer(2,1);
				}
				else if(Switch){
					_SwitchState = SettingUp;
					Pause();
					SSD1306DisplaySetupStart();
					SSD1306DisplayUpdataState(_SwitchState);
					SSD1306DisplayMenuPointer(0,1);
					_MenuState = RPM;
				}
				break;
			case Time:
				if(direction == 1){
					_MenuState = Stop;
					SSD1306DisplayMenuPointer(2,1);
				}else if(direction == 2){
					_MenuState = RPM;
					SSD1306DisplayMenuPointer(0,1);
				}
				else if(Switch){
					_SwitchState = SettingUp;
					Pause();
					SSD1306DisplaySetupStart();
					SSD1306DisplayUpdataState(_SwitchState);
					SSD1306DisplayMenuPointer(1,1);
					_MenuState = Time;
					_time = uren;

				}
				break;
			case Stop:
				if(direction == 1){
					_MenuState = RPM;
					SSD1306DisplayMenuPointer(0,1);
				}else if(direction == 2){
					_MenuState = Time;
					SSD1306DisplayMenuPointer(1,1);
				}
				else if(Switch){
					Secs = 0;
					Mins = 0;
					Hours = 0;
					SetTime(&Secs, &Mins, &Hours);

					SSD1306DisplayUpdataTimeSecs(Secs);
					SSD1306DisplayUpdataTimeMins(Mins);
					SSD1306DisplayUpdataTimeHours(Hours);

					rpm = 0;
					UpdateRPM(rpm);
					SSD1306DisplayUpdataRPM(rpm);

					_SwitchState = Updating;
					SSD1306DisplayUpdataState(_SwitchState);
					SSD1306DisplaySetupStart();
					SSD1306DisplayMenuPointer(3,1);
					_MenuState = Start;
				}
				break;
		}
		break;
	}
}














