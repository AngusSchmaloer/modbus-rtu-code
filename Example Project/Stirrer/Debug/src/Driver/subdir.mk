################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/Driver/I2C.c \
../src/Driver/SSD1306.c \
../src/Driver/TIM.c \
../src/Driver/UART.c 

OBJS += \
./src/Driver/I2C.o \
./src/Driver/SSD1306.o \
./src/Driver/TIM.o \
./src/Driver/UART.o 

C_DEPS += \
./src/Driver/I2C.d \
./src/Driver/SSD1306.d \
./src/Driver/TIM.d \
./src/Driver/UART.d 


# Each subdirectory must supply rules for building sources it contributes
src/Driver/%.o: ../src/Driver/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DSTM32 -DSTM32F1 -DSTM32F103C8Tx -DDEBUG -DSTM32F103xB -DUSE_HAL_DRIVER -DUSE_FULL_LL_DRIVER -I"/home/angusschmaloer/workspace/Stirrer/HAL_Driver/Inc/Legacy" -I"/home/angusschmaloer/workspace/Stirrer/inc" -I"/home/angusschmaloer/workspace/Stirrer/CMSIS/device" -I"/home/angusschmaloer/workspace/Stirrer/CMSIS/core" -I"/home/angusschmaloer/workspace/Stirrer/HAL_Driver/Inc" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


