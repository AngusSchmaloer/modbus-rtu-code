/*
 * SSD1306.h
 *
 *  Created on: Dec 8, 2017
 *      Author: angusschmaloer
 */

#ifndef DRIVER_SSD1306_H_
#define DRIVER_SSD1306_H_

#include "Driver/I2C.h"

/* Display Functions */
void SSD1306_Init			(I2C_HandleTypeDef *I2C_HandleStruct);
int  SSD1306WriteDisplay	(uint8_t * data, int fill);
int  SSD1306DisplayBuffer	(int _buffer);
int  SSD1306Display			(uint8_t *pData, int size, int xmin, int row);
void SSD1306ClearDisplay	(void);


#endif /* DRIVER_SSD1306_H_ */
