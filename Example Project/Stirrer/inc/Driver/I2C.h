/*
 * I2C.h
 *
 *  Created on: Dec 8, 2017
 *      Author: angusschmaloer
 */

#ifndef DRIVER_I2C_H_
#define DRIVER_I2C_H_

#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_i2c.h"
#include "stm32f1xx_hal_rcc.h"

#include "stm32f1xx_hal_conf.h"
#include "stm32f1xx_hal_rcc_ex.h"
#include "stm32f1xx_hal_cortex.h"

#include <stm32f1xx_it.h>
//#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
//#include <string.h>

#define MAXQUEUESIZE			 		  200


typedef struct I2C_Queue_typedef I2C_Queue_struct;


enum Direction{
	transmit,
	receive
};

enum I2C_Flag{
	Waiting,
	Transmitted,
	Received,
	error
};

struct I2C_Queue_typedef{
	I2C_Queue_struct 	* Next_I2C_Queue_struct;
	I2C_HandleTypeDef 	* hi2c;
	uint16_t 			  DevAddress;
	uint8_t 			* pData;
	uint16_t 			  Size;
	volatile bool		  TimeoutTimer;
	enum Direction		  Direction;
	enum I2C_Flag		* Flag;
};

I2C_HandleTypeDef *I2C1_I2C_HandleStruct;


void I2C_Init(I2C_HandleTypeDef *I2C_HandleStruct);

void I2C_Master_Timeout_Handler(void);
int  I2C_Master_Communication(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, enum Direction Direction, enum I2C_Flag *Flag);



#endif /* DRIVER_I2C_H_ */
