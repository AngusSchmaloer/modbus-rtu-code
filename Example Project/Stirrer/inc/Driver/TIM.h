/*
 * TIM.h
 *
 *  Created on: Dec 14, 2017
 *      Author: angusschmaloer
 */

#ifndef DRIVER_TIM_H_
#define DRIVER_TIM_H_

#include "stm32f1xx_hal.h"

#define	PWMPORT 		GPIOA
#define	PWMPin  		GPIO_PIN_0
#define	PolarityPORT 	GPIOB
#define	PolarityPin  	GPIO_PIN_0
#define	EnablePORT 		GPIOB
#define	EnablePin  		GPIO_PIN_1

void TIM2_Init(TIM_HandleTypeDef *htim);
void TIM4_Init(TIM_HandleTypeDef *htim);


#endif /* DRIVER_TIM_H_ */
