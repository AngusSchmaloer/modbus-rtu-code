/*
 * Display.h
 *
 *  Created on: Dec 8, 2017
 *      Author: angusschmaloer
 */

#ifndef USER_DISPLAY_H_
#define USER_DISPLAY_H_

#include "Driver/SSD1306.h"

void SSD1306DisplaySetupStart	(void);
void SSD1306DisplaySetupStop	(void);
void SSD1306DisplaySetupRPM		(void);
void SSD1306DisplaySetupState	(void);
void SSD1306DisplaySetupTime	(void);

void SSD1306DisplayUpdataState		(int state);
void SSD1306DisplayMenuPointer		(int state, int selection);
void SSD1306DisplayUpdataRPM		(signed int RPM);
void SSD1306DisplayUpdataTimeSecs	(uint8_t time);
void SSD1306DisplayUpdataTimeMins	(uint8_t time);
void SSD1306DisplayUpdataTimeHours	(uint8_t time);

void SSD1306DisplaySetupRIP(bool high);

#endif /* USER_DISPLAY_H_ */
