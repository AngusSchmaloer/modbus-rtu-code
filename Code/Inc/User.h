/*
 * User.h
 *
 *  Created on: Dec 8, 2017
 *      Author: angusschmaloer
 */

#ifndef USER_H_
#define USER_H_

#include "User/Display.h"
#include "User/RotaryEncoder.h"
#include "User/Timer.h"

#include "Driver/UART.h"
#include "User/ModbusRTU_Master.h"
#include "User/ModbusRTU_Slave.h"

#if !defined UART1_MODBUS_MASTER
#define UART1_MODBUS_MASTER    			0
#endif

#if !UART1_MODBUS_MASTER
#define UART1_MODBUS_SLAVE       		1
#endif

#if !defined I2C1_Master
#define I2C1_Master    					1
#endif

I2C_HandleTypeDef 				I2C_HandleStruct;
UART_HandleTypeDef				UART_HandleStruct;
ModbusRTU_Master_Data_Typedef 	ModbusRTU_Master_Data_Struct;
ModbusRTU_Slave_Data_Typedef 	ModbusRTU_Slave_Data_Struct;

void User_UART_Init(UART_HandleTypeDef * UART_HandleStruct);
void User_ModbusRTU_Master_Init(ModbusRTU_Master_Data_Typedef *ModbusRTU_Master_Data_Struct, UART_HandleTypeDef *UART_HandleStruct);
void User_ModbusRTU_Slave_Init(ModbusRTU_Slave_Data_Typedef *ModbusRTU_Slave_Data_Struct, UART_HandleTypeDef *UART_HandleStruct);

void Display_Init		(I2C_HandleTypeDef *I2C_HandleStruct);
void Timer_Init			(void);
void RotaryEncoder_Init	(void);

#endif /* USER_H_ */
