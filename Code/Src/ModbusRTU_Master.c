/*
 * ModbusRTU_Master.c
 *
 *  Created on: Nov 24, 2017
 *      Author: angusschmaloer
 *
 *      This is the Modbus RTU Master driver file.
 *      The Modbus RTU Master uses a queue structure that handles the queue automatically.
 */

#include "User/ModbusRTU_Master.h"

/*
 * Modbus RTU Master handle functions
 */
void ModbusRTU_Master_HandleQueue							(ModbusRTU_Master_Data_Typedef *ModbusRTU_Master_Data_Struct);
int  ModbusRTU_Master_Handle_function					    (ModbusRTU_Master_Data_Typedef *ModbusRTU_Master_Data_Struct);

void ModbusRTU_Master_Execute_Command						(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t *StringLen);
void ModbusRTU_Master_Read_Command							(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t *StringLen);
void ModbusRTU_Master_Read_Write_Multiple_Registers			(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t *StringLen);
void ModbusRTU_Master_Write_Single_Register					(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t *StringLen);
void ModbusRTU_Master_Write_Multiple_Registers				(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t *StringLen);
void ModbusRTU_Master_Write_Single_Coil						(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t *StringLen);
void ModbusRTU_Master_Write_Multiple_Coils					(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t *StringLen);

int  ModbusRTU_Master_Handle_Command						(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex);
int  ModbusRTU_Master_Handle_Read_Holding_Registers			(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex);
int  ModbusRTU_Master_Handle_Read_Coils						(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex);
int  ModbusRTU_Master_Handle_Read_Discrete_Inputs			(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex);
int  ModbusRTU_Master_Handle_Read_Write_Multiple_Registers	(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex);
int  ModbusRTU_Master_Handle_Write_Single_Register			(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex);
int  ModbusRTU_Master_Handle_Write_Multiple_Registers		(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex);
int  ModbusRTU_Master_Handle_Write_Single_Coil				(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex);
int  ModbusRTU_Master_Handle_Write_Multiple_Coils			(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex);
int  ModbusRTU_Master_Handle_Report_Slave_ID				(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex);

/*
 * The init function
 * This function stores the user struct, given in the parameter, into the drivers stored struct
 */
int ModbusRTU_Master_Init(ModbusRTU_Master_Data_Typedef *ModbusRTU_Master_Data_Struct){
	if(ModbusRTU_Master_Data_Struct == NULL){
		return 1;
	}
	if(ModbusRTU_Master_Data_Struct->UART_Handle_Struct->Instance == USART1){
		UART1_ModbusRTU_Master_Data_Struct = ModbusRTU_Master_Data_Struct;

	}else if(ModbusRTU_Master_Data_Struct->UART_Handle_Struct->Instance == USART2){
		UART2_ModbusRTU_Master_Data_Struct = ModbusRTU_Master_Data_Struct;
	}
	return 0;
}

/*
 * The modbus master timeout handler function
 * This Functions needs to be Called every ms. (systick)
 */
void ModbusRTU_Master_Timeout_Handler(){
	int NumberCnt;
	/* Get the current systick */
	uint32_t Current = HAL_GetTick();

	/* check if uart1 is initialized */
	if(UART1_ModbusRTU_Master_Data_Struct != NULL){
		/* Get the current queue counter */
		NumberCnt = UART1_ModbusRTU_Master_Data_Struct->QueueCounter;
		/* Check if the status is transmitted, if the state is aything else then transmitted, then do notting  */
		if(((UART1_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->status == transmitted){

			/* check if the current - saved is greater than the given max timeout time */
			if((Current - UART1_ModbusRTU_Master_Data_Struct->TimeoutCount) > ((UART1_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->timeout)
			{
				/* Up the times failed and total times failed */
				((UART1_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->times_failed++;
				((UART1_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->total_times_failed++;
				/* check if there are retries left */
				if(((UART1_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->times_failed >= ((UART1_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->retries){
					/* if none retries are left, then set status to failed and up the counter */
					((UART1_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->status = failed;
					UART1_ModbusRTU_Master_Data_Struct->QueueCounter++;
					/* If the command is not continuous, then set active to false */
					if(!((UART1_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->continuous){
						((UART1_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->active = false;
					}
					/* stop the DMA and handle the next queue object */
					HAL_UART_DMAStop(UART1_ModbusRTU_Master_Data_Struct->UART_Handle_Struct);
					ModbusRTU_Master_HandleQueue(UART1_ModbusRTU_Master_Data_Struct);
				}else{
					/* if there are retries left, then stop the DMA, send it again and set the status to processing */
					HAL_UART_DMAStop(UART1_ModbusRTU_Master_Data_Struct->UART_Handle_Struct);
						HAL_UART_Transmit_DMA(UART1_ModbusRTU_Master_Data_Struct->UART_Handle_Struct, UART1_ModbusRTU_Master_Data_Struct->Data_String, UART1_ModbusRTU_Master_Data_Struct->StringLen);		//Enable DMA UART transfer with interrupt
					((UART1_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->status = processing;
				}
			}
		}
	}

	/* check if uart2 is initialized */
	if(UART2_ModbusRTU_Master_Data_Struct != NULL){
		/* Get the current queue counter */
		NumberCnt = UART2_ModbusRTU_Master_Data_Struct->QueueCounter;
		/* Check if the status is transmitted, if the state is aything else then transmitted, then do notting  */
		if(((UART2_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->status == transmitted){

			/* check if the current - saved is greater than the given max timeout time */
			if((Current - UART2_ModbusRTU_Master_Data_Struct->TimeoutCount) > ((UART2_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->timeout)
			{
				/* Up the times failed and total times failed */
				((UART2_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->times_failed++;
				((UART2_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->total_times_failed++;
				/* check if there are retries left */
				if(((UART2_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->times_failed >= ((UART2_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->retries){
					/* if none retries are left, then set status to failed and up the counter */
					((UART2_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->status = failed;
					UART2_ModbusRTU_Master_Data_Struct->QueueCounter++;
					/* If the command is not continuous, then set active to false */
					if(!((UART2_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->continuous){
						((UART2_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->active = false;
					}
					/* stop the DMA and handle the next queue object */
					HAL_UART_DMAStop(UART2_ModbusRTU_Master_Data_Struct->UART_Handle_Struct);
					ModbusRTU_Master_HandleQueue(UART2_ModbusRTU_Master_Data_Struct);
				}else{
					/* if there are retries left, then stop the DMA, send it again and set the status to processing */
					HAL_UART_DMAStop(UART2_ModbusRTU_Master_Data_Struct->UART_Handle_Struct);
						HAL_UART_Transmit_DMA(UART2_ModbusRTU_Master_Data_Struct->UART_Handle_Struct, UART2_ModbusRTU_Master_Data_Struct->Data_String, UART2_ModbusRTU_Master_Data_Struct->StringLen);		//Enable DMA UART transfer with interrupt
					((UART2_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + NumberCnt)->status = processing;
				}
			}
		}
	}
}

/*
 * Modbus RTU Master Start Queue functions.
 * This functions starts the handling of the queue
 */
void ModbusRTU_Master_Start_Queue	(ModbusRTU_Master_Data_Typedef *ModbusRTU_Master_Data_Struct){
	ModbusRTU_Master_Data_Struct->QueueCounter = 0;
	ModbusRTU_Master_HandleQueue(ModbusRTU_Master_Data_Struct);
}

/*
 * This function handles the queue.
 * It sends one command in the queue
 */
void ModbusRTU_Master_HandleQueue(ModbusRTU_Master_Data_Typedef *ModbusRTU_Master_Data_Struct){
	static bool UARTBUSY;
	uint8_t *NumberCnt;
	NumberCnt = &ModbusRTU_Master_Data_Struct->QueueCounter;

	/* if there are no queue objects left, then return*/
	if(ModbusRTU_Master_Data_Struct->queueSize <= ModbusRTU_Master_Data_Struct->QueueCounter){
		return;
	}

	/* while the current is not active, make the next one the current one */
	while(!((ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + *NumberCnt)->active){
		ModbusRTU_Master_Data_Struct->QueueCounter++;
		if(ModbusRTU_Master_Data_Struct->queueSize <= ModbusRTU_Master_Data_Struct->QueueCounter){
			return;
		}
	}

	/* set the status and reset the times_failed*/
	((ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + *NumberCnt)->status = processing;
	((ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + *NumberCnt)->times_failed = 0;

	/* Execute the queue object */
	ModbusRTU_Master_Execute_Command(((ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + *NumberCnt), ModbusRTU_Master_Data_Struct->Data_String, &ModbusRTU_Master_Data_Struct->StringLen);


//	static int count;
//	if(ModbusRTU_Master_Data_Struct->StringLen != 8){
//		count++;
//	}

	/* Transmit the string, if the transfer goes wrong then set the UART on busy, if the UART is already busy, then abort the UART */
	if(HAL_UART_Transmit_DMA(ModbusRTU_Master_Data_Struct->UART_Handle_Struct, ModbusRTU_Master_Data_Struct->Data_String, ModbusRTU_Master_Data_Struct->StringLen) != HAL_OK){ //Enable DMA UART transfer with interrupt
		if(!UARTBUSY){
			UARTBUSY = true;
		}else{
			HAL_UART_Abort(ModbusRTU_Master_Data_Struct->UART_Handle_Struct);
			UARTBUSY = false;
		}
	}
}
/*
 * Executes the queue object and writes the string
 */
void ModbusRTU_Master_Execute_Command				(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t *StringLen){
	/* reset the StringLen */
	*StringLen = 0;
	/* write the slave address and the fucntion code of the queue object */
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Slave_Address;
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Function_Codes;

	/* depending on the function code, further write the string */
	switch(ModbusRTU_Queue_Struct->Function_Codes){
	case Read_Coils 			:						//All these three use the same function
	case Read_Discrete_Inputs 	:
	case Read_Holding_Registers :
		ModbusRTU_Master_Read_Command					(ModbusRTU_Queue_Struct, UART_String, StringLen);
		break;
	case Write_Single_Coil :
		ModbusRTU_Master_Write_Single_Coil				(ModbusRTU_Queue_Struct, UART_String, StringLen);
		break;
	case Write_Multiple_Coils :
		ModbusRTU_Master_Write_Multiple_Coils			(ModbusRTU_Queue_Struct, UART_String, StringLen);
		break;
	case Write_Single_Register :
		ModbusRTU_Master_Write_Single_Register			(ModbusRTU_Queue_Struct, UART_String, StringLen);
		break;
	case Write_Multiple_Registers :
		ModbusRTU_Master_Write_Multiple_Registers		(ModbusRTU_Queue_Struct, UART_String, StringLen);
		break;
	case Read_Write_Multiple_Registers :
		ModbusRTU_Master_Read_Write_Multiple_Registers	(ModbusRTU_Queue_Struct, UART_String, StringLen);
		break;
	case Report_Slave_ID :
		//We only need to send the "Header" and CRC
		break;
	}

	/* Calculate the CRC and add it to the string */
	uint16_t crctemp = ModRTU_CRC(UART_String, *StringLen);
	(*(UART_String + (*StringLen)++)) = crctemp & 0x00FF;
	(*(UART_String + (*StringLen)++)) = (crctemp & 0xFF00) >> 8;
}

/*
 * This function is for sending a read command.
 * because all read commands are build the same way, we can use the same function
 */
void ModbusRTU_Master_Read_Command					(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t *StringLen){
	/* write the address and the quantity in the string */
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Address >> 8;
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Address & 0xFF;
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Quantity >> 8;
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Quantity & 0xFF;
}

void ModbusRTU_Master_Read_Write_Multiple_Registers			(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t *StringLen){

}

void ModbusRTU_Master_Write_Single_Register			(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t *StringLen){
	/* Make master command and transmit*/
	/* write the address and the data in the string */
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Address >> 8;
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Address & 0xFF;
	(*(UART_String + (*StringLen)++)) = *ModbusRTU_Queue_Struct->data16 >> 8;
	(*(UART_String + (*StringLen)++)) = *ModbusRTU_Queue_Struct->data16 & 0xFF;
}

void ModbusRTU_Master_Write_Multiple_Registers		(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t *StringLen){
	/* Make master command and transmit*/
	/* write the address, the quantity and the bytecount in the string */
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Address >> 8;
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Address & 0xFF;
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Quantity >> 8;
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Quantity & 0xFF;
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Quantity*2; //byte count

	/* write the data in the string */
	for(int x = 0; x < ModbusRTU_Queue_Struct->Quantity; x++){
		(*(UART_String + (*StringLen)++)) = *(ModbusRTU_Queue_Struct->data16 + x) >> 8;
		(*(UART_String + (*StringLen)++)) = *(ModbusRTU_Queue_Struct->data16 + x) & 0xFF;
	}
}

void ModbusRTU_Master_Write_Single_Coil				(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t *StringLen){
	/* Make master command and transmit*/
	/* write the address and the data in the string */
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Address >> 8;
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Address & 0xFF;
	(*(UART_String + (*StringLen)++)) = *ModbusRTU_Queue_Struct->data1 *0xFF;
	(*(UART_String + (*StringLen)++)) = 0x00;
}

void ModbusRTU_Master_Write_Multiple_Coils			(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t *StringLen){
	/* Make master command and transmit*/
	/* write the address, the quantity adnt eh bytecount in the string */
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Address >> 8;
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Address & 0xFF;
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Quantity >> 8;
	(*(UART_String + (*StringLen)++)) = ModbusRTU_Queue_Struct->Quantity & 0xFF;
	uint8_t Bytecount   = (ModbusRTU_Queue_Struct->Quantity+7)/8;
	(*(UART_String + (*StringLen)++)) = Bytecount; //byte count

	/* write the data in the string */
    for(int x = 0; x < Bytecount; x++){
        for(int y = 0; (y < (ModbusRTU_Queue_Struct->Quantity - (8*x))) && (y < 8); y++){
            (*(UART_String + (*StringLen))) |= *(ModbusRTU_Queue_Struct->data1 + (8*x) + y) << y;
        }
        (*StringLen)++;
    }
}

#define string    ModbusRTU_Master_Data_Struct->Data_String
#define stringlen ModbusRTU_Master_Data_Struct->StringLen

/*
 * Handles the responds form the slaves.
 * called by the UART interrupt.
 * Handles the queue.
 * ModbusRTU_Master_Handle_function handles the actual data
 */
void ModbusRTU_Master_Handler(ModbusRTU_Master_Data_Typedef * ModbusRTU_Master_Data_Struct, int strlen){
	int Error_Code;
	stringlen = strlen;		//strlen = the total length of the string, if strlen = 8, then string[7] is the last one

	/* set the status of the queue object */
	((ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + ModbusRTU_Master_Data_Struct->QueueCounter)->status = received;

	/* Handle the responds, each function code needs different handling */
	Error_Code = ModbusRTU_Master_Handle_function(ModbusRTU_Master_Data_Struct);

	/* set teh error code */
	((ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + ModbusRTU_Master_Data_Struct->QueueCounter)->ErrorCode = Error_Code;

	/* if an error occurs, then the message is failed*/
	if(Error_Code != 0){
		((ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + ModbusRTU_Master_Data_Struct->QueueCounter)->status = failed;
	}

	/* if the message is not continuous, then set the active bool on false */
	if(!((ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + ModbusRTU_Master_Data_Struct->QueueCounter)->continuous){
		((ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + ModbusRTU_Master_Data_Struct->QueueCounter)->active = false;
	}

	/* up the counter and call the queue handler again to handle the next queue */
	ModbusRTU_Master_Data_Struct->QueueCounter++;
	ModbusRTU_Master_HandleQueue(ModbusRTU_Master_Data_Struct);
	/*
	 * End
	 * */
}

/*
 * Handles the actual data from the received data.
 * Checks if the respond is as expected
 */
int  ModbusRTU_Master_Handle_function(ModbusRTU_Master_Data_Typedef * ModbusRTU_Master_Data_Struct){
	int rxindex = 0;
	uint16_t CalculatedCRCvalue;
	uint16_t receivedCRCvalue;

	/* Check if the address in the response is the same as the address the command was written to */
	if((*(string + rxindex++)) != ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct->Slave_Address){
		return 5; //Adress Error
	}

	/* Calculate the CRC check and check if its the same as the received */


	/*calculate the CRC value */
	CalculatedCRCvalue = ModRTU_CRC(string , 8 - 2);
	receivedCRCvalue = ((*(string +  (stringlen - 1))) << 8) + (*(string +  (stringlen - 2)));

	/* Compare the CRC value to the given CRC Value in the string */
	if(CalculatedCRCvalue != receivedCRCvalue){
		return 6; //CRC check
	}

	/* Check if the function code in the response is the same as the function code that was written */
	if((*(string + rxindex)) != ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct->Function_Codes){
		/* if the function is not the same, then check if an exception code was send*/
		if((*(string + rxindex)) >= (0x80 + ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct->Function_Codes)){
			/* return the exception code */
			return (*(string + rxindex++));
		}
		return 7; //Wrong Function
	}
	/* Handle the function */
	return ModbusRTU_Master_Handle_Command(((ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct) + ModbusRTU_Master_Data_Struct->QueueCounter), ModbusRTU_Master_Data_Struct->Data_String, rxindex);
}


int ModbusRTU_Master_Handle_Command (ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex){
	rxindex++;
	switch(ModbusRTU_Queue_Struct->Function_Codes){
	case Read_Coils :
		return ModbusRTU_Master_Handle_Read_Coils(ModbusRTU_Queue_Struct, UART_String, rxindex);
		break;
	case Write_Single_Coil :
		return ModbusRTU_Master_Handle_Write_Single_Coil(ModbusRTU_Queue_Struct, UART_String, rxindex);
		break;
	case Write_Multiple_Coils :
		return ModbusRTU_Master_Handle_Write_Multiple_Coils(ModbusRTU_Queue_Struct, UART_String, rxindex);
		break;
	case Read_Discrete_Inputs :
		return ModbusRTU_Master_Handle_Read_Discrete_Inputs(ModbusRTU_Queue_Struct, UART_String, rxindex);
		break;
	case Read_Holding_Registers :
		return ModbusRTU_Master_Handle_Read_Holding_Registers(ModbusRTU_Queue_Struct, UART_String, rxindex);
		break;
	case Write_Single_Register :
		return ModbusRTU_Master_Handle_Write_Single_Register(ModbusRTU_Queue_Struct, UART_String, rxindex);
		break;
	case Write_Multiple_Registers :
		return ModbusRTU_Master_Handle_Write_Multiple_Registers(ModbusRTU_Queue_Struct, UART_String, rxindex);
		break;
	case Read_Write_Multiple_Registers :
		return ModbusRTU_Master_Handle_Read_Write_Multiple_Registers(ModbusRTU_Queue_Struct, UART_String, rxindex);
		break;
	case Report_Slave_ID :
		return ModbusRTU_Master_Handle_Report_Slave_ID(ModbusRTU_Queue_Struct, UART_String, rxindex);
		break;
	}
	return 10;
}

/*
 * Handles the read holding registers respond
 */
int  ModbusRTU_Master_Handle_Read_Holding_Registers(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex){
	/* Check if the Quantity is OK */
	if(ModbusRTU_Queue_Struct->Quantity * 2 != (*(UART_String + rxindex++))){
		return 8;
	}
	/* Read the data from the string and safe it into the holding registers */
	for(int x = 0; x  < ModbusRTU_Queue_Struct->Quantity; x++){
		(*(ModbusRTU_Queue_Struct->data16 + x)) =  (*(UART_String + rxindex++)) << 8;
		(*(ModbusRTU_Queue_Struct->data16 + x)) += (*(UART_String + rxindex++)) & 0xff;
	}
	return 0;
}

/*
 * Handles the read Coils respond
 */
int  ModbusRTU_Master_Handle_Read_Coils(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex){
	/* Calc bytecount */
	int Bytecount = (ModbusRTU_Queue_Struct->Quantity+7)/8;

	/* Check if the bytecount is OK */
	if(Bytecount != (*(UART_String + rxindex++))){	// Byte Count
		return 8;
	}

	/* Read the data from the string and safe it into the Coils */
	for(int x = 0; x  < Bytecount; x++){
        for(int y = 0; (y < (ModbusRTU_Queue_Struct->Quantity - (8*x))) && (y < 8); y++){
    		(*(ModbusRTU_Queue_Struct->data1 + (8*x) + y)) = ((*(UART_String + rxindex)) >> y) && 0x01;
        }
        (rxindex)++;
	}
	return 0;
}

/*
 * Handles the read Discrete Inputs respond
 */
int  ModbusRTU_Master_Handle_Read_Discrete_Inputs(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex){
	/* Calc bytecount */
	int Bytecount = (ModbusRTU_Queue_Struct->Quantity+7)/8;

	/* Check if the bytecount is OK */
		if(Bytecount != (*(UART_String + rxindex++))){
		return 9;
	}

	/* Read the data from the string and safe it into the Discrete inputs */
	for(int x = 0; x  < ModbusRTU_Queue_Struct->Quantity; x++){
		(*(ModbusRTU_Queue_Struct->data8 + x)) =  (*(UART_String + rxindex++));
	}
	return 0;
}

/*
 * Handles the Write Single Register respond
 */
int  ModbusRTU_Master_Handle_Write_Single_Register(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex){
	uint16_t Address;
	uint16_t Value;
	/* read the address*/
	Address =  (*(UART_String + rxindex++)) << 8;
	Address += (*(UART_String + rxindex++));
	/* Check if the address is OK */
	if(ModbusRTU_Queue_Struct->Address != Address){
		return 8;
	}
	/* read the Value*/
	Value =  (*(UART_String + rxindex++)) << 8;
	Value += (*(UART_String + rxindex++));
	/* Check if the value is OK */
	if(*ModbusRTU_Queue_Struct->data16 != Value){
		return 8;
	}
	return 0;
}

/*
 * Handles the Write Multiple Registers respond
 */
int  ModbusRTU_Master_Handle_Write_Multiple_Registers(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex){
	uint16_t Address;
	uint16_t Quantity;
	/* read the address*/
	Address =  (*(UART_String + rxindex++)) << 8;
	Address += (*(UART_String + rxindex++));
	/* Check if the address is OK */
	if(ModbusRTU_Queue_Struct->Address != Address){
		return 8;
	}
	/* read the Quantity*/
	Quantity =  (*(UART_String + rxindex++)) << 8;
	Quantity += (*(UART_String + rxindex++));
	/* Check if the Quantity is OK */
	if(ModbusRTU_Queue_Struct->Quantity != Quantity){
		return 8;
	}
	return 0;
}

int  ModbusRTU_Master_Handle_Read_Write_Multiple_Registers(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex){
	return 0;
}

/*
 * Handles the Report Slave ID respond
 */
int  ModbusRTU_Master_Handle_Report_Slave_ID(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex){
	/* Read */
	uint8_t Bytecount;
	uint8_t SlaveID;
	uint8_t RunIndicatorStatus;
	uint8_t AdditionalData;
	Bytecount = 			(*(UART_String + rxindex++));
	SlaveID =  				(*(UART_String + rxindex++));
	RunIndicatorStatus =	(*(UART_String + rxindex++));
	AdditionalData =   		(*(UART_String + rxindex++));
	return 0;
}

/*
 * Handles the Write Single Coil respond
 */
int  ModbusRTU_Master_Handle_Write_Single_Coil(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex){
	uint16_t Address;
	uint16_t Value;
	/* read the address*/
	Address =  (*(UART_String + rxindex++)) << 8;
	Address += (*(UART_String + rxindex++));
	/* Check if the address is OK */
	if(ModbusRTU_Queue_Struct->Address != Address){
		return 8;
	}
	/* read the Value*/
	Value =  (*(UART_String + rxindex++)) << 8;
	Value += (*(UART_String + rxindex++));
	/* Check if the Value is OK */
	if(*ModbusRTU_Queue_Struct->data16 != Value){
		return 8;
	}
	return 0;
}

/*
 * Handles the Write Multiple Coils respond
 */
int  ModbusRTU_Master_Handle_Write_Multiple_Coils(ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct, uint8_t * UART_String, uint8_t rxindex){
	uint16_t Address;
	uint16_t Quantity;
	/* read the address*/
	Address =  (*(UART_String + rxindex++)) << 8;
	Address += (*(UART_String + rxindex++));
	/* Check if the address is OK */
	if(ModbusRTU_Queue_Struct->Address != Address){
		return 8;
	}
	/* read the Quantity*/
	Quantity =  (*(UART_String + rxindex++)) << 8;
	Quantity += (*(UART_String + rxindex++));
	/* Check if the Quantity is OK */
	if(ModbusRTU_Queue_Struct->Quantity != Quantity){
		return 8;
	}
	return 0;
}

