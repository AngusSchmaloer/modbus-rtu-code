/*
 * ModbusRTU_Slave.c
 *
 *  Created on: Nov 24, 2017
 *      Author: angusschmaloer
 *
 *  Modbus RTU Slave driver.
 *  This driver has an init,start and a stop function that the user can use.
 *  The other functions are used for the callback functions
 *  This library implements a total of 2 static modbusses
 */

#include "User/ModbusRTU_Slave.h"

/* the slave handle function, called by the UART interrupt */
int ModbusRTU_Slave_Handle_Command					(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex, enum Function_codes _Function_codes);

/* The functions for handling the different requests */
int ModbusRTU_Slave_Read_Holding_Registers			(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex);
int ModbusRTU_Slave_Read_Coils						(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex);
int ModbusRTU_Slave_Read_Discrete_Inputs			(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex);
int ModbusRTU_Slave_Write_Single_Register			(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex);
int ModbusRTU_Slave_Write_Multiple_Registers		(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex);
int ModbusRTU_Slave_Read_Write_Multiple_Registers	(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex);
int ModbusRTU_Slave_Report_Slave_ID					(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex);
int ModbusRTU_Slave_Write_Single_Coil				(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex);
int ModbusRTU_Slave_Write_Multiple_Coils			(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex);

/*
 * This is the UART init function.
 * This function stores the user struct, given in the parameter, into the drivers stored struct
 */
int ModbusRTU_Slave_Init(ModbusRTU_Slave_Data_Typedef *ModbusRTU_Slave_Data_Struct){
	/* check parameter */
	if(ModbusRTU_Slave_Data_Struct == NULL){return 1;}

	/* Sets the different structs */
	if(ModbusRTU_Slave_Data_Struct->UART_Handle_Struct->Instance == USART1){
		UART1_ModbusRTU_Slave_Data_Struct = ModbusRTU_Slave_Data_Struct;
	}else if(ModbusRTU_Slave_Data_Struct->UART_Handle_Struct->Instance == USART2){
		UART2_ModbusRTU_Slave_Data_Struct = ModbusRTU_Slave_Data_Struct;
	}
	return 0;
}

/* start the modbus slave by starting the uart receive */
void ModbusRTU_Slave_Start(ModbusRTU_Slave_Data_Typedef *ModbusRTU_Slave_Data_Struct){
	HAL_UART_Receive_DMA(ModbusRTU_Slave_Data_Struct->UART_Handle_Struct , ModbusRTU_Slave_Data_Struct->Data_String, 256);
}

/* Stop the modbus slave */
void ModbusRTU_Slave_Stop(ModbusRTU_Slave_Data_Typedef *ModbusRTU_Slave_Data_Struct){
	HAL_UART_DMAStop(ModbusRTU_Slave_Data_Struct->UART_Handle_Struct);
}

/* Two simple defines to save typing work and make it more clear to read */
#define string    	ModbusRTU_Slave_Data_Struct->Data_String
#define stringlen 	ModbusRTU_Slave_Data_Struct->StringLen
/*
 * Modbus RTU Slave Handle function.
 * This function is called by the UART interrupt.
 * This functions handles the received data that is stored in the ModbusRTU_Slave_Data_Struct.
 * It forms an answer based on the request.
 */
int ModbusRTU_Slave_Handler(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int strlen){
	int stringIndex = 0;
	uint16_t CalculatedCRCvalue;
	uint16_t receivedCRCvalue;
	bool Broadcast = false;

	stringlen = strlen;						//strlen = the total length of the string, if strlen = 8, then string[7] is the last one
	enum Function_codes Function_codes;		//enum for the function codes that will be red from the received string

	/* 3 is the minimum for a modbus command */
	if(stringlen < 3){
		ModbusRTU_Slave_Data_Struct->Error_Counters.CPT2++;
		return 1;
	}

	/*calculate the CRC value */
	CalculatedCRCvalue = ModRTU_CRC(string , 8 - 2);
	receivedCRCvalue = ((*(string +  (stringlen - 1))) << 8) + (*(string +  (stringlen - 2)));

	/* Compare the CRC value to the given CRC Value in the string */
	if(CalculatedCRCvalue != receivedCRCvalue){
		ModbusRTU_Slave_Data_Struct->Error_Counters.CPT2++;
		return 2;
	}

	/* up an counter modbus slave */
	ModbusRTU_Slave_Data_Struct->Error_Counters.CPT1++;

	/* Check if the address is correct*/
	if((*(string)) != ModbusRTU_Slave_Data_Struct->Slave_Address || (*(string)) == 0x00){
		return 3;
	}
	/* up an counter modbus slave */
	ModbusRTU_Slave_Data_Struct->Error_Counters.CPT4++;

	/* if its a broadcast(0x00) then upp another counter*/
	if((*(string + stringIndex++)) == 0x00){
		Broadcast = true;
		ModbusRTU_Slave_Data_Struct->Error_Counters.CPT5++;
	}

	/* read the function code */
	Function_codes = (*(string + stringIndex++));

	/* Handle the function request */
	int exeptioncode = ModbusRTU_Slave_Handle_Command(ModbusRTU_Slave_Data_Struct, &stringIndex, Function_codes);

	/* if it is a broadcast, then don't send a responds */
	if(Broadcast){
		return 4;
	}

	/* if everything is correct the exeptioncode will be 0
	 * otherwise, send the exeptioncode back*/
	if(exeptioncode != 0){
		/* reset the stringIndex */
		stringIndex = 1;
		/* rewrite/overwrite the string with the exeption code*/
		(*(string + stringIndex++)) = 0x80 + Function_codes;
		(*(string + stringIndex++)) = exeptioncode;
	}
	/* Add the CRC check to the string */
	uint16_t crctemp = ModRTU_CRC(string, stringIndex);
	(*(string + stringIndex++)) = crctemp & 0x00FF;
	(*(string + stringIndex++)) = (crctemp & 0xFF00) >> 8;

	/* transit the string */
	HAL_UART_Transmit_DMA(ModbusRTU_Slave_Data_Struct->UART_Handle_Struct, string, stringIndex);
	return 0;
}

/*
 * Handles the different functions
 * parms
 * 		ModbusRTU_Slave_Data_Struct:		the data for the modbus
 * 		stringIndex:						counter for in the string pointer
 * 		_Function_codes						The requested function
 * */
int ModbusRTU_Slave_Handle_Command(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex, enum Function_codes _Function_codes){
	switch(_Function_codes){
	case Read_Coils :
		return ModbusRTU_Slave_Read_Coils(ModbusRTU_Slave_Data_Struct, stringIndex);
		break;
	case Write_Single_Coil :
		return ModbusRTU_Slave_Write_Single_Coil(ModbusRTU_Slave_Data_Struct, stringIndex);
		break;
	case Write_Multiple_Coils :
		return ModbusRTU_Slave_Write_Multiple_Coils(ModbusRTU_Slave_Data_Struct, stringIndex);
		break;
	case Read_Discrete_Inputs :
		return ModbusRTU_Slave_Read_Discrete_Inputs(ModbusRTU_Slave_Data_Struct, stringIndex);
		break;
	case Read_Holding_Registers :
		return ModbusRTU_Slave_Read_Holding_Registers(ModbusRTU_Slave_Data_Struct, stringIndex);
		break;
	case Write_Single_Register :
		return ModbusRTU_Slave_Write_Single_Register(ModbusRTU_Slave_Data_Struct, stringIndex);
		break;
	case Write_Multiple_Registers :
		return ModbusRTU_Slave_Write_Multiple_Registers(ModbusRTU_Slave_Data_Struct, stringIndex);
		break;
	case Read_Write_Multiple_Registers :
		return ModbusRTU_Slave_Read_Write_Multiple_Registers(ModbusRTU_Slave_Data_Struct, stringIndex);
		break;
	case Report_Slave_ID :
		return ModbusRTU_Slave_Report_Slave_ID(ModbusRTU_Slave_Data_Struct, stringIndex);
		break;
	default:
		ModbusRTU_Slave_Data_Struct->Error_Counters.CPT1++;
		return 1;
		//exception 1;
	}
	return 0;
}

#define Data 		(*(string + (*stringIndex)++))		//This is the data to the current uint8_t and automatically increment the stringIndex (counter that walks through the string pointer)

/*
 * Read Slave coils function
 * parms
 * 		ModbusRTU_Slave_Data_Struct:		the data for the modbus
 * 		stringIndex:						counter for in the string pointer
 * */
int ModbusRTU_Slave_Read_Coils(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex){    // (*(UARTx_String + stringIndex))    = 8;
	static uint16_t CoilsAddress;
	static uint16_t CoilsQuantity;
	uint8_t TempData;

		/* read the coils address */
		CoilsAddress = (Data << 8);
		CoilsAddress += Data;

		/* read the coils quantity */
		CoilsQuantity = (Data << 8);
		CoilsQuantity += Data;

		/* Check if the quantity is correct */
		if(0x0001 > CoilsQuantity || CoilsQuantity > 0x07D0){
			return 3;
		}
		/* Check if the address is correct */
		if(0x0000 > CoilsAddress || CoilsAddress + CoilsQuantity >= ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Coils_Size){
			return 2;
		}

		/* reset the stringIndex for writing in the string */
		(*stringIndex) = 2;
		/* Write the byte count */
		Data = (CoilsQuantity + 8 - 1) / 8; //byte count

		/* Read the coils and write it in the data string */
		for(int x = 0; x < (CoilsQuantity + 7) / 8; x++){
		    TempData = 0x00;
		    for(int y = 0; y < 8 && (CoilsQuantity - 8*(x-1)) < y; y++){
		    	TempData +=  *(ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Modbus_Slave_Coils + CoilsAddress + (8*x) + y) << y;
		    }
		    Data = TempData; // coils data
		}
		return 0;
}
/*
 * Write Slave coils function
 * parms
 * 		ModbusRTU_Slave_Data_Struct:		the data for the modbus
 * 		stringIndex:						counter for in the string pointer
 * */
int ModbusRTU_Slave_Write_Single_Coil(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex){
	static uint16_t CoilsAddress;
	static uint16_t CoilsValue;

		/* read the coils address */
		CoilsAddress = (Data << 8);
		CoilsAddress += Data;

		/* read the coils value */
		CoilsValue = (Data << 8);
		CoilsValue += Data;

		/* Check if the value is correct */
		if(CoilsValue != 0x0000 && CoilsValue != 0xff00){
			return 3;
		}
		/* Check if the address is correct */
		if(0x0000 > CoilsAddress || CoilsAddress >= ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Coils_Size){
			return 2;
		}

		/* write in the coil */
		*(ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Modbus_Slave_Coils + CoilsAddress) = CoilsValue == 0xFF00 ? true : false;
		return 0;
}

/*
 * Write multiple Slave coils function
 * parms
 * 		ModbusRTU_Slave_Data_Struct:		the data for the modbus
 * 		stringIndex:						counter for in the string pointer
 * */
int ModbusRTU_Slave_Write_Multiple_Coils(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex){
	static uint16_t CoilsAddress;
	static uint16_t CoilsQuantity;
	static uint8_t  CoilsValue;
	static uint8_t  ByteCount;

		/* read the coils address */
		CoilsAddress = (Data << 8);
		CoilsAddress += Data;

		/* read the coils quantity */
		CoilsQuantity = (Data << 8);
		CoilsQuantity  += Data;

		/* Check if the quantity is correct */
		if(0x0001 > CoilsQuantity || CoilsQuantity > 0x07D0){
			return 3;
		}
		/* Check if the address is correct */
		if(0x0000 > CoilsAddress || CoilsAddress + CoilsQuantity >= ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Coils_Size){
			return 2;
		}

		ByteCount = Data;

	/* Write the multiple holding registers */
	for(int y = 0; y < ByteCount; y++){
		CoilsValue = Data;
		for(int x = 0; x < 8 && 0 < CoilsQuantity; x++){
			*(ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Modbus_Slave_Coils + x + CoilsAddress + y*8) = (CoilsValue >> x) % 2;
			CoilsQuantity--;
		}
	}
	(*stringIndex) = 6;
	return 0;
}

/*
 * Write Single Slave Register function
 * parms
 * 		ModbusRTU_Slave_Data_Struct:		the data for the modbus
 * 		stringIndex:						counter for in the string pointer
 * */
int ModbusRTU_Slave_Write_Single_Register(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex){
	static uint16_t RegistersAddress;
	static uint16_t RegistersValue;

		/* read the coils address */
		RegistersAddress = (Data << 8);
		RegistersAddress += Data;

		/* read the coils value */
		RegistersValue = (Data << 8);
		RegistersValue += Data;

		/* Check if the value is correct */
		if(0x0000 > RegistersValue || RegistersValue > 0xFFFF){
			return 3;
		}
		/* Check if the address is correct */
		if(0x0000 > RegistersAddress || RegistersAddress >= ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Holding_Registers_size){
			return 2;
		}

		/* Write the single holding registers */
		*(ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Modbus_Slave_Holding_Registers + RegistersAddress) = RegistersValue;
		return 0;
}

/*
 * Write Multiple Slave Registers function
 * parms
 * 		ModbusRTU_Slave_Data_Struct:		the data for the modbus
 * 		stringIndex:						counter for in the string pointer
 * */
int ModbusRTU_Slave_Write_Multiple_Registers(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex){
	static uint16_t RegistersAddress;
	static uint16_t RegistersQuantity;
	static uint16_t RegistersValue;

	/* read the coils address */
	RegistersAddress = (Data << 8);
	RegistersAddress += Data;

	/* read the coils Quantity */
	RegistersQuantity = (Data << 8);
	RegistersQuantity += Data;

	/* Check if the Quantity is correct */
	if(0x0001 > RegistersQuantity || RegistersQuantity > 0x007B){
		return 3;
	}
	/* Check if the value is correct */
	if(0x000 > RegistersAddress || RegistersAddress + RegistersQuantity >= ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Holding_Registers_size){
		return 2;
	}

	//ByteCount = Data; But we don't use bytecount in calculations, because its Quantity * 2
	(*stringIndex)++;

	/* Write in the multiple registers */
	for(int i = 0; i < RegistersQuantity; i++){
		RegistersValue  = (Data << 8);
		RegistersValue += Data;
		*(ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Modbus_Slave_Holding_Registers + RegistersAddress + i) = RegistersValue;
	}
	(*stringIndex) = 6;
	return 0;
}

/*
 * Read Slave Discrete inputs function
 * parms
 * 		ModbusRTU_Slave_Data_Struct:		the data for the modbus
 * 		stringIndex:						counter for in the string pointer
 * */
int ModbusRTU_Slave_Read_Discrete_Inputs(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex){
	static uint16_t InputAddress;
	static uint16_t InputQuantity;

	/* read the coils address */
	InputAddress = (Data << 8);
	InputAddress += Data;

	/* read the coils Quantity */
	InputQuantity = (Data << 8);
	InputQuantity += Data;

	/* Check if the Quantity is correct */
	if(0x0001 > InputQuantity || InputQuantity > 0x07D0){
		return 3;
	}
	/* Check if the Address is correct */
	if(0x000 > InputAddress || InputAddress + InputQuantity >= ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Discrete_Input_Size){
		return 2;
	}

	/* reset the stringIndex for writing */
	(*stringIndex) = 2;

	Data = (InputQuantity * 2);

	for(int x = 0; x < InputQuantity; x--){
		Data = (*(ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Modbus_Slave_Discrete_Inputs + InputAddress + x) >> 8);
		Data = (*(ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Modbus_Slave_Discrete_Inputs + InputAddress + x) & 0xFF);
	}
	return 0;
}
/*
 * Read and write Slave multiple registers function
 * parms
 * 		ModbusRTU_Slave_Data_Struct:		the data for the modbus
 * 		stringIndex:						counter for in the string pointer
 * */
int ModbusRTU_Slave_Read_Write_Multiple_Registers(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex){
	static uint16_t ReadRegistersAddress;
	static uint16_t ReadRegistersQuantity;
	static uint16_t WriteRegistersAddress;
	static uint16_t WriteRegistersQuantity;
	static uint16_t WriteRegistersValue;
	uint8_t  bytecount;

	/* read the read register address */
	ReadRegistersAddress = (Data << 8);
	ReadRegistersAddress += Data;

	/* read the read register Quantity */
	ReadRegistersQuantity = (Data << 8);
	ReadRegistersQuantity += Data;

	/* read the write register address */
	WriteRegistersAddress = (Data << 8);
	WriteRegistersAddress += Data;

	/* read the write register Quantity */
	WriteRegistersQuantity = (Data << 8);
	WriteRegistersQuantity += Data;

	/* read the byte count */
	bytecount = Data;

	/* Check if the Quantities are correct */
	if((0x0001 > ReadRegistersQuantity  || ReadRegistersQuantity  > 0x007D) ||
	   (0x0001 > WriteRegistersQuantity || WriteRegistersQuantity > 0x0079) ||
	   (bytecount != WriteRegistersQuantity)){
		return 3;
	}
	/* Check if the addresses are correct */
	if((0x000 > ReadRegistersAddress   || ReadRegistersAddress  + ReadRegistersQuantity  >= ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Holding_Registers_size) ||
	   (0x000 > WriteRegistersAddress  || WriteRegistersAddress + WriteRegistersQuantity >= ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Holding_Registers_size)){
		return 2;
	}
	/*  Write the string data into the regs */
	for(int x = 0; x < WriteRegistersQuantity; x++){
		WriteRegistersValue = Data << 8;
		WriteRegistersValue += Data;
		*(ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Modbus_Slave_Holding_Registers + WriteRegistersAddress) = WriteRegistersValue;
	}

	/* reset the stringIndex for writing in the string */
	(*stringIndex) = 2;

	/* Read from the regs and put them into the string */
	Data =  ReadRegistersQuantity + ReadRegistersQuantity;
	for(int x = 0; x < ReadRegistersQuantity; x ++){
		Data = (*(ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Modbus_Slave_Holding_Registers + WriteRegistersAddress + x) >> 8);
		Data = (*(ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Modbus_Slave_Holding_Registers + WriteRegistersAddress + x) & 0xFF);
	}
	return 0;
}

/*
 * Report Slave ID function
 * parms
 * 		ModbusRTU_Slave_Data_Struct:		the data for the modbus
 * 		stringIndex:						counter for in the string pointer
 * */
int ModbusRTU_Slave_Report_Slave_ID(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex){
	(*stringIndex)++;											//Skip Byte count, we add it later
	Data = ModbusRTU_Slave_Data_Struct->Slave_Address;
	Data = 0xFF;  												//mean it is running
	//Data = Aditional Data;
	(*(string + 2)) = 2;										//size of SlaveAddress(1) + 0xFF(1) + Aditional Data(0) = 2
	return 0;
}

/*
 * Read Slave Discrete inputs function
 * parms
 * 		ModbusRTU_Slave_Data_Struct:		the data for the modbus
 * 		stringIndex:						counter for in the string pointer
 * */
int ModbusRTU_Slave_Read_Holding_Registers(ModbusRTU_Slave_Data_Typedef * ModbusRTU_Slave_Data_Struct, int *stringIndex){
	static uint16_t RegistersAddress;
	static uint16_t RegistersQuantity;

	/* read the register address */
	RegistersAddress = (Data << 8);
	RegistersAddress += Data;

	/* read the register Quantity */
	RegistersQuantity = (Data << 8);
	RegistersQuantity += Data;

	/* Check if the Quantities is correct */
	if(0x0001 > RegistersQuantity || RegistersQuantity > 0x007D){
		return 3;
	}
	/* Check if the Address is correct */
	if(0x000 > RegistersAddress || RegistersAddress + RegistersQuantity >= ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Holding_Registers_size){
		return 2;
	}

	/* reset the stringIndex for writing in the string */
	(*stringIndex) = 2;

	/* write the quantity */
	Data = RegistersQuantity*2;

	/* write the holding registers into the string */
	for(int x = 0; x < RegistersQuantity; x++){
		Data = (*(ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Modbus_Slave_Holding_Registers + RegistersAddress + x) >> 8);
		Data = (*(ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data->Modbus_Slave_Holding_Registers + RegistersAddress + x) & 0xFF);
	}
	return 0;
}

